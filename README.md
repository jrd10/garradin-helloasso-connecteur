> Le projet a été repris pas l'équipe Garradin qui s'appelle désormais Paheko, https://paheko.cloud/. Suivre l'actualité du projet sur leur site.

# Le projet (Archives)

Analyse fonctionnelle (et plus) d'un connecteur entre Garradin(.eu), une excellente application pour la gestion des associations L1901 (membres, comptabilité, autres) et des systèmes de paiement en ligne comme HelloAsso (et d'autres). 

Nous sommes à la recherche de dévs (même débutants ou encore étudiants) qui pourraient contribuer au projet

# Références (Archives)

- Garradin : https://garradin.eu

- Site Garradin développeur : https://fossil.kd2.org/garradin/doc/trunk/doc/index.md
- Site pour les API : https://fossil.kd2.org/garradin/wiki?name=API
- Sites des extensions : https://fossil.kd2.org/garradin/wiki?name=Extensions

- HelloAsso : https://www.helloasso.com
- La page HelloAsso sur les API, notamment pour trouver votre clé : https://centredaide.helloasso.com/s/article/api-comment-fonctionne-l-api-helloasso?name=api-comment-fonctionne-l-api-helloasso

- Site pour les API Swagger (Attention, utiliser la v5) : https://api.helloasso.com/v5/swagger/ui/index

- HelloAsso sur gitlab : https://github.com/HelloAsso
    - Voir le projet pour  : https://github.com/HelloAsso/php-notification-handler-sample


## Sites de plugins pour Garradin par des développers indépendants (Archives)

- Liste sur le site de Garradin : https://fossil.kd2.org/garradin/wiki?name=Extensions
    - Ramoloss : https://gitlab.com/ramoloss
    - Nicolas Frery : https://github.com/nfrery/Modules-Garradin
    - Olav_2 : https://framagit.org/Olav_2

## L'analyse fonctionnelle de base (Archives)

![Analyse Fonctionnelle Globale](https://gitlab.com/jrd10/garradin-helloasso-connecteur/-/raw/master/Garradin-HelloAsso_AF_Gloable_v01.png "Analyse Fonctionnelle Globale sur diagrams.net")

- Sur https://diagrams.net.

## Détails des données (Archives)

### Sur HelloAsso (Archives)

- Les données des contributeurs sont accessibles sur le panneau d'administration de l'entité.

![Panneau données contributeurs](https://gitlab.com/jrd10/garradin-helloasso-connecteur/-/raw/master/img/HelloAsso-Contributeur_Formation_20210415.png "Image pour formation, ne pas diffuser")

#### Champs de l'export total

- `Numéro`,	`Campagne`,	`Type`,	`Désignation`,	`Date`,	`Montant`,	`Code Promo`,	`Statut`,	`Moyen de paiement`,	`Attestation`,	`Reçu`,	`Don Anonyme`,	`Nom`,	`Prénom`,	`Société`,	`Adresse`,	`Code Postal`,	`Ville`,	`Pays`,	`Email`,	`Date de naissance`,	`Commentaire`,	`Numéro de reçu`, 	`Nom acheteur`,	`Prénom acheteur`,	`Adresse acheteur`,	`Code Postal acheteur`,	`Ville acheteur`,	`Pays acheteur`.

- Les données issues de l'export d'HelloAsso

- À noter que les données `acheteur` correspondent au propriétaire du moyen de paiement qui n'est pas nécessairement le membre ou le donateur.

### Sur Garradin (Version 1.0.6) (Archives)

- Sur Garradin, les données des adhésions ont deux espaces :

    - L'espace de 'membre'
    - L'espace 'Activtés et cotisations' où l'on doit déclarer :
        - L''**activité**', comme une campagne d'adhésion : année, type d'adhésion,
        - et les '**tarifs**' : tarif normal, tarif étudiant, ...

#### Les champs de Garradin liés à l'espace 'Membres' (Archives)

- La "Fiche de membre" permet de lister toutes les champs de base (champ prédéfini) et ceux qui sont créés (champ personnalisé)
1. `numero` : clé de chaque membre. dans l'ordre d'inscription. Une clé / un membre éffacé et l'indice devient inaccessible
2. `nom` : nom complet
3. `email` : clé pour se connecter au Sites
4. `adresse` : numéro, rue, hameau
5. `code_postal` 
6. `ville`
7. `pays`
8. `telephone`
9. `lettre_infos` : case à cocher pour accepter de recevoir les courriels envoyés par l'instance Garradin

- Chez tricassinux, j'ai rajouté :
10. `pseudo` : un pseudonyme
11. `notes` : un champ commentaire où le membre peut parlé de lui et des ses motivations.

- Pour chaque de ces champs (réglable dans : Configuration > Onglet Fiche des membres)
1. `Type` : Case à cocher, ...
2. `Titre` (obligatoire)
3. `Aide` : un texte d'aide pour l'utilisateur
4. `Caché pour les membres` : case à cocher
5. `Modifiable par les membres` 
6. `Champ obligatoire`
7. `Numéro de colonne dans la liste des membres` : la colonne dans le tableau de bord des membres
Dans la zone 'Mot de passe' :
9. `Caché pour les membres` : pas visible par les membres dans leur espace personnel
10. `Modifiable par les membres` : les membres pourront changer leur mot de passe si coché
11. `Champ obligatoire `: à la création du membre 


#### Les champs de Garradin liés à l'espace 'Activtés et cotisations' et 'Tarifs' (Archives)

- Les champs liés à 'Activités et cotisations' :
    - Champ `Libellé` (obligatoire)
    - Champ `Description`
    - Champ `Période de validité`
        - `Pas de période (Cotisation ponctuelle)`
        - `En nombre de jours`
            - `Durée de validité`
        - `Période définie (date à date)`
            - `Date de début`
            - `Date de fin`

- Pour chaque champ ci-dessus, il existe un ou plusieurs tarifs. Les champs liés au tarif :
    - Champ `Libellé` (Obligatoire)
    - Champ `Description`
    - Montant de la cotisation
        - Champ `Gratuite ou prix libre`
        - Champ ` Montant fixe ou prix libre conseillé`
            - Champ `Montant`
        - Champ `Montant variable`
            - Champ `Formule de calcul`
    - Case à cocher `Enregistrer en comptabilité`
    - Zone enregistrer en comptabilité :
        - `Exercice` (Obligatoire) : Sélectionner un Exercice
        - `Compte à utiliser` (Obligatoire) : Sélectionner.

- Il existe d'autres champs :
    - `À jour et payés`
    - `Inscription expirée`
    - `En attente de règlement`
- À voir si ces champs seront d'une utilité.

### Matrice de correspondance des champs

- Travail à faire :)

### Cas des autres connexions (autres banques, ...)

- Travail à faire :)

## Vison de l'écosystème Garradin, HelloAsso et autres

![raphique de l'écosystème autour de Garradin](https://gitlab.com/jrd10/urbanisation-si/-/raw/master/modeles/Urba-SI-Anonyme_Comptabilite-Sauvegarde_v01.png "Graphique sur Diagrams.net")


## Forkez, contribuez (Archives)

- N'hésitez pas à contribuer et/ou partager vos expériences. 

## Licence (Archives)

- **GNU GENERAL PUBLIC LICENSE** 
- Voir la [licence](https://gitlab.com/jrd10/garradin-helloasso-connecteur/-/blob/master/LICENSE "Licence").
